=========================================
Page yang tersedia:

1. Home page - menyediakan informasi mengenai lokasi toilet yang available dan tidak available. Pada halaman ini juga terdapat tombol Login untuk masuk ke halaman Login.

2. Login page - halaman ini adalah pintu akses untuk masuk ke halaman user/admin. Yang perlu dimasukkan adalah username dan password, lalu tekan tombol Login.

3. User page - pada halaman ini terdapat informasi rata-rata penggunaan air, dan user dapat melaporkan kebersihan/kerusakan (toggle button), mengisi lokasi toilet, dan mengisi keluhan pada kolom komentar. Setelah itu tekan tombol Send untuk mengirimnya.

4. Admin page (Dashboard and Scheduling) - pada halaman Dashboard, admin dapat melihat grafik penggunaan air dan tingkat polusi pada air. Dari halaman ini, admin dapat memilih halaman Schedule atau Logout.
Kemudian, pada halaman Schedule, admin dapat melihat laporan yang masuk, dan memberi command pada janitor/engineer untuk mengerjakan/menyelesaikan tugas berdasarkan laporan yang masuk. Lalu, admin dapat menekan tombol Send Command untuk mengirimkan notifikasi tugas kepada janitor/engineer. Dan pada halaman ini juga admin dapat menekan tombol Logout. Jika tombol Logout ditekan, maka akan kembali pada halaman Home.

Untuk menjalankan halaman ini, hal yang perlu dilakukan adalah:
1. Menyalakan XAMPP
2. Mengakses halaman home, dengan cara localhost/bath/Home.html

Database yang dipakai saat ini masih lokal, dengan phpmyadmin.